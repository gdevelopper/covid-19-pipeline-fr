from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash import BashOperator

default_args = {
    "owner": "g-developper",
    "retries": 5,
    "retry_delay": timedelta(minutes=2)
}

with DAG(
    dag_id="bash-dag-v3",
    description="this dag contains only bash operators",
    start_date=datetime(2022, 1, 1),
    end_date=datetime(2023, 3, 4),
    schedule_interval=None, # this gives me the possibility to start this dag manually
    default_args=default_args,
    tags=["bash"]
) as dag:
    task1 = BashOperator(
        task_id="first-task",
        bash_command="echo hello world, this is a gentle message!"
    )

    task2 = BashOperator(
        task_id="second_task",
        bash_command="echo I am the second task"
    )

    task3 = BashOperator(
        task_id="third-task",
        bash_command="echo I am the third task"
    )

    task1 >> [task2, task3]