from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash import BashOperator

default_args = {
    "owner" : "g-developper",
    "retries": 2,
    "retry_delay": timedelta(minutes=1)
}

"""
catchup : execute all the dag run for the first time in the past.
back-fil : execute old dag runs for failure for example 
"""
with DAG(
    dag_id="dag-with-catch-up",
    start_date=datetime(2023, 3, 1),
    end_date=datetime(2023, 3, 6),
    default_args=default_args,
    schedule_interval="@daily",
    catchup=True,
    tags=["bash"]
) as dag:

    task1 = BashOperator(
        task_id="bash-operator-with-catchup",
        bash_command="echo I am a bash operator with catchup"
    )

    task1