from airflow.models import DAG
from airflow.operators.python import PythonOperator
from sqlalchemy import create_engine
from datetime import datetime
import requests
from io import StringIO
import pandas as pd

dag = DAG(dag_id="Covid-19",
          description='this pipeline provides daily covid-19 contamination statistics',
          schedule_interval="0 0 * * *",
          start_date=datetime(2021, 3, 27),
          catchup=False)
def log(message):
    print(f"[ETL] {message}")

log("Hello to covid DAG ETL")

def get_covid_db_connection() -> object:
    host = 'postgres'
    password = 'airflow'
    user = "airflow"
    db = "covid-19"
    return create_engine(f"postgresql://{user}:{password}@{host}/{db}")


def extract():
    """
    This function query covid.ourworldindata.org to get Covid-19 data, and returns the results as a dataframe.
    :return: Pandas DataFrame, containing data about Covid-19
    """
    try:
        log("[Start the extracting ")
        url = "https://covid.ourworldindata.org/data/owid-covid-data.csv"
        http_resp = requests.get(url)
        if http_resp.ok:
            csv_file = StringIO(http_resp.content.decode("utf-8"))
            df = pd.read_csv(csv_file)
            log(df.info())
            log("End of the extracting")
            return df
        raise Exception("Http request not satisfied")
    except Exception as e:
        log(f"[ERROR] Exception occurred while loading data : {e}")
        raise e


def transform(data: pd.DataFrame) -> pd.DataFrame:
    """

    :param data:
    :return:
    """
    try:
        # Get the last date loaded to covid database.
        log("Start of the transforming")
        covid_connection = get_covid_db_connection()
        data.date = pd.to_datetime(data.date, infer_datetime_format=True)
        last_date_sql = '''
                        SELECT year, month, day
                        FROM dim_time
                        ORDER BY make_date(year, month, day) DESC
                        FETCH FIRST 1 ROW ONLY 
                    '''
        last_date = pd.read_sql(last_date_sql, covid_connection)

        if len(last_date) == 0:
            return data

        # Select only new dates that are not already loaded into the covid database.
        last_date = datetime(last_date.year[0], last_date.month[0], last_date.day[0])
        data = data[data.date > last_date]
        log("End of the transforming")
        return data
    except Exception as e:
        log(f"[ERROR] Exception occurred while transforming data : {e}")
        raise e


def load(data):
    covid_con = get_covid_db_connection()

    def _load_time(dates):
        log("    Start loading time")
        columns = ["year", "month", "day", "hour", "week_day", "quarter", "date"]

        # Structure new dates.
        dates = pd.concat(
            [dates.dt.year, dates.dt.month, dates.dt.day, dates.dt.hour, dates.dt.weekday, dates.dt.quarter, dates],
            axis=1)
        dates.columns = columns
        dates.drop_duplicates(inplace=True)
        update_dates = dates[columns[:-1]]

        # Upload new dates to the dim_time table.
        if len(update_dates):
            update_dates.drop_duplicates(inplace=True)
            update_dates.to_sql("dim_time", covid_con, if_exists='append', index=False)
            return dates.date
        else:
            log(f"All dates are up to date")
        log("    End loading time")

    def _load_country(data):
        log("    Start loading countries")
        columns = ["iso_code", "continent", "location"]

        # Read data from dim_country table.
        db_countries = pd.read_sql("SELECT * FROM dim_country", covid_con)[columns]

        # Structure new countries.
        countries = list(data.groupby(columns).groups.keys())
        countries = pd.DataFrame(countries, columns=columns)
        countries.drop_duplicates(inplace=True)

        # Get new countries
        location_set = set(countries.location)
        db_location_set = set(db_countries.location)
        diff = location_set.difference(db_location_set)
        countries_update = countries[countries.location.isin(diff)]

        # Upload new countries to the dim_country table.
        if len(countries_update):
            countries_update.drop_duplicates(inplace=True)
            countries_update.to_sql("dim_country", covid_con, if_exists='append', index=False)
        else:
            log(f"All countries are up to date")
        log("    End of loading countries")

    def _load_test_unit(tests_units):
        log("    Start loading test unites")
        columns = ["test_unite"]
        # Read data from dim_test_unit table.
        db_tests = pd.read_sql("SELECT * FROM dim_test_unit", covid_con)[columns]

        # Structure new test_units.
        tests = pd.DataFrame(tests_units.unique(), columns=columns)
        tests = tests.where(tests.notnull(), None)

        # Get new test unites.
        tests_set = set(tests.test_unite)
        db_tests_set = set(db_tests.test_unite)
        diff = tests_set.difference(db_tests_set)
        tests_update = tests[tests.test_unite.isin(diff)]

        # Upload new tests to the dim_test_unit.
        if len(tests_update):
            tests_update.drop_duplicates(inplace=True)
            tests_update.to_sql('dim_test_unit', covid_con, if_exists="append", index=False)
        else:
            log(f"All test unites are up to date")
        log("    End loading test unites")

    def _load_covid_statistics(data):
        log("    Start loading statistics")
        # get ids from dim_time table
        db_dates = pd.read_sql("select * from dim_time", covid_con)
        dates = pd.to_datetime(db_dates[["year", 'month', 'day', 'hour']])
        db_dates = pd.concat([db_dates, dates], axis=1)
        db_dates.columns = ["id", "year", "day", "month", "hour", "week_day", "quarter", "date"]
        db_dates = db_dates[["id", "date"]]
        db_dates.columns = ["time_id", "date"]
        data["date"] = pd.to_datetime(data.date)
        data = pd.merge(data, db_dates, on="date", how='left')

        # get ids from dim_country
        db_countries = pd.read_sql("select * from dim_country", covid_con)
        db_countries.columns = ["country_id", "iso_code", "location", "continent"]
        data = pd.merge(data, db_countries, on=["iso_code", "location", "continent"], how='left')

        # get ids from dim_test_unit
        db_test_unites = pd.read_sql("select * from dim_test_unit", covid_con)
        db_test_unites.columns = ["test_unit_id", "tests_units"]
        data = pd.merge(data, db_test_unites, on="tests_units")

        # Get the new fact rows
        data.drop(columns=["date", "iso_code", "location", "continent", "tests_units"], inplace=True)
        data.to_sql("fact_covid_statistics", con=covid_con, if_exists="append", index=False)
        log("    End of loading statistics")
    try:
        log("Start of the loading")
        if len(data) == 0:
            log(f"There is no data to load today")
            return

        # Load dimensions tables
        _load_time(data.date)
        _load_country(data)
        _load_test_unit(data.tests_units)

        # Load Fact table
        _load_covid_statistics(data)
        log("End od the loading")
    except Exception as e:
        log(f"[ERROR] Exception occurred while loading data {e}")
        raise e


def etl():
    data = extract()
    data = transform(data)
    load(data)

# python-operator
etl_task = PythonOperator(task_id="etl_covid",
                          python_callable=etl,
                          dag=dag,
                          owner="Ghiles")