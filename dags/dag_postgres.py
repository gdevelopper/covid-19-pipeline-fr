from datetime import datetime, timedelta
from airflow import DAG
from airflow.providers.postgres.operators.postgres import PostgresOperator

default_args = {
    "owner": "g-developper",
    "retries": 2,
    "retry_delay": timedelta(minutes=1)
}

with DAG(
    dag_id="dag-for-postgres-update",
    default_args=default_args,
    start_date=datetime(2023,3,1),
    end_date=datetime(2023, 3, 6),
    schedule_interval=None,
    catchup=False,
    tags=["postgres"]
) as dag:
    task_create_table = PostgresOperator(
        task_id="create-table-rentals-01",
        postgres_conn_id="postgres-localhost-canada",
        sql="""
            CREATE TABLE IF NOT EXISTS rental (
                rental_name text primary key,
                n_bed_rooms int,
                link varchar(250),
                address varchar(250),
                price float
            )
        """
    )

    rental_name = "my_rental_1"

    task_delete_duplicates = PostgresOperator(
        task_id="delete-duplicates",
        postgres_conn_id="postgres-localhost-canada",
        sql=f"""
            delete from rental 
            where rental_name = '{{ rental_name }}'
        """
    )

    task_insert_value = PostgresOperator(
        task_id="insert-new-value",
        postgres_conn_id="postgres-localhost-canada",
        sql=f"""
            insert into rental (rental_name, n_bed_rooms, link, address, price)
            values ('{{ rental_name }}', 2, 'link', 'paris 17', 2000.0)
        """
    )

    task_create_table >> task_delete_duplicates >> task_insert_value