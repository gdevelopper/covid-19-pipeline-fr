create database "covid-19";

comment on database "covid-19" is 'This data base contains all the covid-19 measurements ';

CREATE TABLE "fact_covid_statistics" (
  "country_id" int,
  "time_id" int,
  "test_unit_id" int,
  "total_cases" decimal,
  "new_cases" decimal,
  "new_cases_smoothed" decimal,
  "total_deaths" decimal,
  "new_deaths" decimal,
  "new_deaths_smoothed" decimal,
  "total_cases_per_million" decimal,
  "new_cases_per_million" decimal,
  "new_cases_smoothed_per_million" decimal,
  "total_deaths_per_million" decimal,
  "new_deaths_per_million" decimal,
  "new_deaths_smoothed_per_million" decimal,
  "reproduction_rate" decimal,
  "icu_patients" decimal,
  "icu_patients_per_million" decimal,
  "hosp_patients" decimal,
  "hosp_patients_per_million" decimal,
  "weekly_icu_admissions" decimal,
  "weekly_icu_admissions_per_million" decimal,
  "weekly_hosp_admissions" decimal,
  "weekly_hosp_admissions_per_million" decimal,
  "new_tests" decimal,
  "total_tests" decimal,
  "total_tests_per_thousand" decimal,
  "new_tests_per_thousand" decimal,
  "new_tests_smoothed" decimal,
  "new_tests_smoothed_per_thousand" decimal,
  "positive_rate" decimal,
  "tests_per_case" decimal,
  "total_vaccinations" decimal,
  "people_vaccinated" decimal,
  "people_fully_vaccinated" decimal,
  "new_vaccinations" decimal,
  "new_vaccinations_smoothed" decimal,
  "total_vaccinations_per_hundred" decimal,
  "people_vaccinated_per_hundred" decimal,
  "people_fully_vaccinated_per_hundred" decimal,
  "new_vaccinations_smoothed_per_million" decimal,
  "stringency_index" decimal,
  "population" decimal,
  "population_density" decimal,
  "median_age" decimal,
  "aged_65_older" decimal,
  "aged_70_older" decimal,
  "gdp_per_capita" decimal,
  "extreme_poverty" decimal,
  "cardiovasc_death_rate" decimal,
  "diabetes_prevalence" decimal,
  "female_smokers" decimal,
  "male_smokers" decimal,
  "handwashing_facilities" decimal,
  "hospital_beds_per_thousand" decimal,
  "life_expectancy" decimal,
  "human_development_index" decimal,
  "excess_mortality" decimal,
  PRIMARY KEY ("country_id", "time_id", "test_unit_id")
);

CREATE TABLE "dim_country" (
  "id" SERIAL PRIMARY KEY,
  "iso_code" varchar,
  "location" varchar,
  "continent" varchar
);

CREATE TABLE "dim_time" (
  "id" SERIAL PRIMARY KEY,
  "year" int,
  "day" int,
  "month" int,
  "hour" int,
  "week_day" int,
  "quarter" int
);

CREATE TABLE "dim_test_unit" (
  "id" SERIAL PRIMARY KEY,
  "test_unite" varchar
);

ALTER TABLE "fact_covid_statistics" ADD FOREIGN KEY ("country_id") REFERENCES "dim_country" ("id");

ALTER TABLE "fact_covid_statistics" ADD FOREIGN KEY ("time_id") REFERENCES "dim_time" ("id");

ALTER TABLE "fact_covid_statistics" ADD FOREIGN KEY ("test_unit_id") REFERENCES "dim_test_unit" ("id");
