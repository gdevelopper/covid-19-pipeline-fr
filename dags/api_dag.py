from airflow.decorators import dag, task
from datetime import datetime, timedelta


default_args = {
    "owner": "g-developper",
    "retries": 5,
    "retry_delay": timedelta(minutes=1)
}

@dag(
    dag_id="greeting-dag-02",
    description="this dag prints a hello world with a name and an age",
    start_date=datetime(2022, 1, 1),
    end_date=datetime(2023, 3, 5),
    schedule_interval=None,
    default_args=default_args,
    tags=["python"]
)
def hello_greeting():

    @task(multiple_outputs=True)
    def get_name() -> str:
        return {"lastname": "denane", "firstname": "ghiles"}

    @task()
    def get_age() -> int:
        return 27

    @task()
    def greet(firstname: str, lastname: str, age: int) -> None:
        print(f"Hello World ! my name is {firstname} {lastname} and I have {age} years old :)")

    name = get_name()
    age = get_age()

    greet(firstname=name["firstname"], lastname=name["lastname"], age=age)

hello_greeting()