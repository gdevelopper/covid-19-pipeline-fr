from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python import PythonOperator

default_args = {
    "owner": "g-developper",
    "retries": 5,
    "retry_delay": timedelta(minutes=1)
}
def get_name(task_instance)-> str:
    task_instance.xcom_push(key="name", value="denane")

def get_age(task_instance) -> None:
    task_instance.xcom_push(key="age", value=12)


def greet(task_instance)-> None:
    name = task_instance.xcom_pull(task_ids="get_name", key="name")
    age = task_instance.xcom_pull(task_ids="get_age", key="age")
    print(f"Hello World ! je my name {name} and I am {age} years old")

with DAG(
    dag_id="python-dag-06",
    description="This is the first python dag",
    start_date=datetime(2020, 1, 1),
    end_date=datetime(2023, 3, 5),
    schedule_interval=None,
    default_args= default_args,
    tags=["python"]
) as dag:

    task1 = PythonOperator(
       task_id="python_op_01",
       python_callable=greet
    )

    task2 = PythonOperator(
        task_id="get_name",
        python_callable=get_name
    )

    task3 = PythonOperator(
        task_id="get_age",
        python_callable=get_age
    )

    [task2, task3] >> task1